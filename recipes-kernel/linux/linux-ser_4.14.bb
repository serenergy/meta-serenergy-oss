SUMMARY = "FSL Community BSP Linux mainline based kernel"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=d7810fab7487fb0aad327b76f1be7cd7"

require linux-ser.inc

SRC_URI = "git://bitbucket.org/serenergy/linux.git;protocol=ssh;branch=4.14.y-fslc-srgy"
SRCREV = "5c11d632dba53787e7db42c1300d9ee54e7679d2"
S = "${WORKDIR}/git"
