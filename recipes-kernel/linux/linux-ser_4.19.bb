SUMMARY = "Mainline kernel package"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=bbea815ee2795b2f4230826c0c6b8814"

require linux-ser.inc

SRC_URI = "git://bitbucket.org/serenergy/linux.git;protocol=ssh;branch=4.19.y-srgy \
           file://0001-can-make-interrupt-level-triggered.patch \
           "
SRCREV = "ea7923bbbeff6988b1727a4816912c0506759180"
S = "${WORKDIR}/git"
