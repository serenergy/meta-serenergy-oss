DESCRIPTION = "U-Boot based on mainline U-Boot used by FSL Community BSP in \
order to provide support for some backported features and fixes, or because it \
was submitted for revision and it takes some time to become part of a stable \
version, or because it is not applicable for upstreaming."
LICENSE = "GPLv2+"

require recipes-bsp/u-boot/u-boot.inc
include u-boot-repo.inc

PROVIDES += "u-boot"

# FIXME: Allow linking of 'tools' binaries with native libraries
#        used for generating the boot logo and other tools used
#        during the build process.
EXTRA_OEMAKE += 'HOSTCC="${BUILD_CC} ${BUILD_CPPFLAGS}" \
                 HOSTLDFLAGS="${BUILD_LDFLAGS}" \
                 HOSTSTRIP=true \
                 PYTHON=nativepython'

PACKAGE_ARCH = "${MACHINE_ARCH}"
COMPATIBLE_MACHINE = "(mxs|mx5|mx6|mx7|vf|use-mainline-bsp)"

PR := "${PR}.7"

do_compile_append() {
    ${B}/tools/mxsboot sd ${B}/u-boot.sb ${B}/u-boot.sd
}

do_install_append() {
    install -m 644 ${B}/u-boot.sd ${D}/boot/u-boot.sd
}
