SUMMARY = "Boot script to be sourced from u-boot"
LICENSE = "MIT"
DEPENDS = "u-boot-mkimage-native"
SRC_URI = "file://boot.txt"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"


S = "${WORKDIR}"
FILES_${PN} = "boot.scr"

do_compile() {
    mkimage -T script -C none -n '${MACHINE} boot script file' -d ${S}/boot.txt ${S}/boot.scr
}

do_deploy[vardepsexclude] = "DATETIME"
do_deploy() {
    install -d ${DEPLOY_DIR_IMAGE}
    install -m0644 ${S}/boot.scr ${DEPLOY_DIR_IMAGE}/boot-${MACHINE}-${DATETIME}.scr
    ln -sf boot-${MACHINE}-${DATETIME}.scr ${DEPLOY_DIR_IMAGE}/boot.scr
}
addtask deploy after do_install before do_build
