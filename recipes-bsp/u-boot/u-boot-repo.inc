SRC_URI = "git://bitbucket.org/serenergy/u-boot-fccp.git;protocol=http;branch=2017.11+fslc"
SRCREV = "b3784b8da0b881b0b6c4e12cb2f205df05a1c4f1"
LIC_FILES_CHKSUM = "file://Licenses/README;md5=a2c678cfd4a4d97135585cad908541c6"
S = "${WORKDIR}/git"
