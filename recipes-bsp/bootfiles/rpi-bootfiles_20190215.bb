DESCRIPTION = "Closed source binary files to help boot the ARM on the BCM2835."
LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://boot/LICENCE.broadcom;md5=4a4d169737c0786fb9482bb6d30401d1"

inherit deploy

SRC_URI = "https://github.com/raspberrypi/firmware/archive/1.${PV}.tar.gz"
SRC_URI[md5sum] = "1e88094a2e6c488826b60ec58a40ccfa"
SRC_URI[sha256sum] = "6b6db5f15c400bc1224266fd44238dd9b2479fd09f8a55e36ccdac2120632f8f"
S = "${WORKDIR}/firmware-1.${PV}"

# This package dosn't require anything to function
INHIBIT_DEFAULT_DEPS = "1"

# Override default architechture (which is arm) and skip the check
PACKAGE_ARCH = "${MACHINE_ARCH}"
INSANE_SKIP_${PN} += "arch"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install() {
    install -d ${D}/boot

    for i in fixup.dat start.elf bootcode.bin; do
        install -m 644 boot/$i ${D}/boot
    done
}

RDEPENDS_${PN} += "rpi-config"
FILES_${PN} = "/boot/"

do_deploy() {
    install -d ${DEPLOYDIR}/${PN}
    install -m644 ${D}/boot/* ${DEPLOYDIR}/${PN}/
}

addtask deploy before do_build after do_install
do_deploy[dirs] += "${DEPLOYDIR}/${PN}"
