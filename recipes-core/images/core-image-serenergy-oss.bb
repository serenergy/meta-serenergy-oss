DESCRIPTION = "Minimal image to bring serenergy HW up with eth support"
LICENSE = "MIT"

include core-image-serenergy-base-hw-oss.inc

IMAGE_INSTALL += " \
    ${MACHINE_EXTRA_RDEPENDS} \
"

TOOLCHAIN_HOST_TASK_append = " nativesdk-protobuf-compiler "
