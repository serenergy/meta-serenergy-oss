SUMMARY = "A small image intended to be used for usb upgrade installer."

IMAGE_FEATURES += " debug-tweaks"
IMAGE_INSTALL = " \
    packagegroup-core-boot \
    ${CORE_IMAGE_EXTRA_INSTALL} \
    e2fsprogs e2fsprogs-e2fsck e2fsprogs-resize2fs \
    util-linux-sfdisk \
    mtd-utils mtd-utils-ubifs mtd-utils-misc \
    u-boot-fslc-fw-utils \
    lighttpd \
    ethtool \
    fstab-usb-installer \
"

IMAGE_LINGUAS = " "

LICENSE = "MIT"

IMAGE_ROOTFS_SIZE = "8192"

inherit core-image
