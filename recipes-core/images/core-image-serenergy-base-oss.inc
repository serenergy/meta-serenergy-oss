DESCRIPTION = "Base serenergy image configuration"
LICENSE = "MIT"

inherit image

#all below is additional packages needed
IMAGE_INSTALL += " \
    libxml2 libxml2-utils \
    libsocketcan \
    can-utils \
    curl \
    ethtool \
    iproute2 \
    libxslt-bin \
    libstdc++ \
    sqlite3 \
    e2fsprogs e2fsprogs-e2fsck e2fsprogs-mke2fs e2fsprogs-tune2fs \
    util-linux \
    json-c \
    protobuf \
    gperftools \
    libupnp \
    agent++ \
    cannelloni \
    net-snmp-server \
    cronie \
    daemonize \
    asio \
    nginx \
    fcgi \
    spawn-fcgi \
    less \
"

EXTRA_IMAGEDEPENDS += " \
    openssl \
"

IMAGE_LINGUAS = " "
IMAGE_ROOTFS_SIZE = "8192"

DEPENDS += " squashfs-tools-native"

EXTRA_IMAGECMD_squashfs  = " \
    -noI -noD -noF -noX \
    -e ${IMAGE_ROOTFS}/boot/* \
    -e ${IMAGE_ROOTFS}/log/* \
    -e ${IMAGE_ROOTFS}/settings/* \
    -e ${IMAGE_ROOTFS}/sdcard/* \
"

WKS_FILE ?= "${MACHINE}.wks.in"
