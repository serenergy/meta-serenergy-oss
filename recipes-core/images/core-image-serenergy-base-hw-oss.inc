DESCRIPTION = "Minimal include to bring up HW"
LICENSE = "MIT"

include core-image-serenergy-base-oss.inc

IMAGE_INSTALL += " \
    packagegroup-core-boot \
    packagegroup-core-ssh-openssh \
    kernel-modules \
    mtd-utils mtd-utils-jffs2 mtd-utils-ubifs mtd-utils-misc \
    libgpiod \
    fstab \
    mount-points \
    fsck-repair-writable \
    persist-machine-id \
    watchdog \
"

