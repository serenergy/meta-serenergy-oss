FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
SRC_URI += " \
        file://Serenergy_Root_CA.crt \
"

do_install_prepend() {
    install -d ${D}${datadir}/ca-certificates/serenergy/
    install -m 0644 ${WORKDIR}/Serenergy_Root_CA.crt ${D}${datadir}/ca-certificates/serenergy/
}

