FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI_append = " \
    file://rauc-mark-good-remount.conf \
"

FILES_${PN}-mark-good_append = " \
    ${systemd_system_unitdir}/rauc-mark-good.service.d/ \
    ${systemd_system_unitdir}/rauc-mark-good.service.d/0-rauc-mark-good-remount-boot.conf \
"

do_install_append () {
    install -d ${D}${systemd_system_unitdir}/rauc-mark-good.service.d/
    install -m 0644 ${WORKDIR}/rauc-mark-good-remount.conf ${D}${systemd_system_unitdir}/rauc-mark-good.service.d/0-rauc-mark-good-remount-boot.conf
}
