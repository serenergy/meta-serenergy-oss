SUMMARY = "fstab for serenergy target platform image"
LICENSE = "PD"
LIC_FILES_CHKSUM = "file://${WORKDIR}/pd_license;md5=7246f848faa4e9c9fc0ea91122d6e680"

SRC_URI = " \
    file://fstab \
    file://pd_license \
"

FILES_${PN} += " \
    ${sysconfdir}/fstab \
"

do_install() {
    install -d ${D}${sysconfdir}
    install -m 0644 ${WORKDIR}/fstab ${D}${sysconfdir}
}
