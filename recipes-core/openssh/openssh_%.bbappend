SRC_URI +="\
file://tmpfiles.d.sshd.conf \
"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
FILES_${PN}_sshd += "${sysconfdir}/tmpfiles.d/sshd.conf"

do_install_append () {
    install -Dm 0644 ${WORKDIR}/tmpfiles.d.sshd.conf ${D}${sysconfdir}/tmpfiles.d/sshd.conf
	sed -i "s/#PermitRootLogin.*/PermitRootLogin yes/g" ${D}${sysconfdir}/ssh/sshd_config_readonly
}
