# Install drop-in for ldconfig.service 
# This changes ldconfig.service to call ldconfig with -C /var/run/ld.so.cache
# to support read only rootfs
# Also install symlink from /etc/ld.so.cache to /var/run/ld.so.cache

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
SRC_URI += "\
    file://ldconfig_ro_fs.conf \
    file://tmpfiles.d.timesyncd.conf \
"

FILES_${PN} += " \
    ${sysconfdir}/systemd/system/ldconfig.service.d/ldconfig_ro_fs.conf \
    ${sysconfdir}/ld.so.cache \
    ${sysconfdir}/tmpfiles.d/timesyncd.conf \
"

do_install_append() {
    ln -s /var/run/ld.so.cache ${D}${sysconfdir}/ld.so.cache
    install -d ${D}${sysconfdir}/systemd/system/ldconfig.service.d/
    install -m 0644 ${WORKDIR}/ldconfig_ro_fs.conf ${D}${sysconfdir}/systemd/system/ldconfig.service.d/
    install -Dm 0644 ${WORKDIR}/tmpfiles.d.timesyncd.conf ${D}${sysconfdir}/tmpfiles.d/timesyncd.conf
    ln -s /settings/systemd/timesyncd.conf.d ${D}${sysconfdir}/systemd/timesyncd.conf.d
}

