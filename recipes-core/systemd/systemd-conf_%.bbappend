FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "\
    file://10-eth0.network \
    file://20-eth1.network \
    file://10-usbeth1.network \
    file://can0.network \
    file://can1.network \
    file://10-canbus.rules \
    file://tmpfiles.d.networkd.conf \
    file://journald_srgy.conf \
    file://networkd.conf \
    file://journal-flush-override.conf \
"

FILES_${PN} += " \
    ${sysconfdir} \
    ${systemd_system_unitdir} \
"

do_install_append() {
    install -d ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/10-eth0.network ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/20-eth1.network ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/10-usbeth1.network ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/can0.network ${D}${sysconfdir}/systemd/network
    install -m 0644 ${WORKDIR}/can1.network ${D}${sysconfdir}/systemd/network
    install -Dm 0644 ${WORKDIR}/10-canbus.rules ${D}${sysconfdir}/udev/rules.d/10-canbus.rules
    install -Dm 0644 ${WORKDIR}/tmpfiles.d.networkd.conf ${D}${sysconfdir}/tmpfiles.d/networkd.conf
    ln -s /settings/network/10-eth0.network.d/ ${D}${sysconfdir}/systemd/network/10-eth0.network.d
    ln -s /settings/network/20-eth1.network.d/ ${D}${sysconfdir}/systemd/network/20-eth1.network.d
    install -Dm 0664 ${WORKDIR}/journald_srgy.conf ${D}${sysconfdir}/systemd/journald.conf.d/srgy.conf
    install -Dm 0664 ${WORKDIR}/networkd.conf ${D}${systemd_system_unitdir}/systemd-networkd.service.d/networkd.srgy.conf
    install -d ${D}${systemd_system_unitdir}/systemd-journal-flush.service.d/
    install -m0644 ${WORKDIR}/journal-flush-override.conf ${D}${systemd_system_unitdir}/systemd-journal-flush.service.d/override.conf
}
