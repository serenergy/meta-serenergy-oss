# Only set default baud rate and no serial TTY device as this is picked up by
# systemd-getty-generator.

# Default is SERIAL_CONSOLES ?= "115200;ttyS0" by ttyS0 is no a tty on our
# machine so it will spam the logs with complaints.

# If SERIAL_CONSOLES is empty serial-getty@.service will not be present and 
# this is required to get a tty from systemd-getty-generator

SERIAL_CONSOLES = "115200"
