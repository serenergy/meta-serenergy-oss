SUMMARY = "Library for implementing snmp++ agents"
HOMEPAGE = "http://www.agentpp.com/"
DEPENDS = "openssl"
LICENSE = "SNMP_PP"
LIC_FILES_CHKSUM = "file://libsnmp.h;endline=26;md5=d8a610efc1cbfcfcbbdf276631198035"
SRC_URI = "http://www.agentpp.com/download/snmp++v${PV}.tar.gz"
SRC_URI[md5sum] = "121e617d771920392db1f5f7e0ce421f"
SRC_URI[sha256sum] = "06a2af44b2976b61667756e3f74604820fce0ef8bfc7f31280308f1eeb7633f3"
S = "${WORKDIR}/snmp++-3.3.11"

PR = "r1"

inherit autotools pkgconfig
