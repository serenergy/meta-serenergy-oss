#!/bin/sh
set -e 

# systemd_machine_id var is not used on feb directly until boot script is updated.
#current_uboot_id="$(fw_printenv -n systemd_machine_id)"
#if [ "${current_uboot_id}" == "" ]; then
#    echo "systemd_machine_id not present in uboot env, setting systemd_machine_id=$(cat /etc/machine-id) now"
#    fw_setenv systemd_machine_id $(cat /etc/machine-id)
#else
#    echo "systemd_machine_id=${current_uboot_id} already present in uboot"
#fi

#because updating feb bootloader is still not in place, simply do this instead.
#if systemd.machine_id not in default_bootargs append it
default_bootargs=$(fw_printenv -n default_bootargs)
if [ "${default_bootargs#*systemd.machine_id}" == "${default_bootargs}" ]; then
    echo "systemd.machine_id arg not present in default_bootargs var in uboot env, appending now"
    fw_setenv default_bootargs "${default_bootargs} systemd.machine_id=$(cat /etc/machine-id)"
else
    echo "systemd.machine_id already present in default_bootargs var in uboot env"
fi
