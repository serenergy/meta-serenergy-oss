#!/bin/sh
set -e

current_uboot_id="$(fw_printenv -n systemd_machine_id || true)"
if [ "${current_uboot_id}" == "" ]; then
    echo "systemd_machine_id not present in uboot env, setting systemd_machine_id=$(cat /etc/machine-id) now"
    fw_setenv systemd_machine_id $(cat /etc/machine-id)
else
    echo "systemd_machine_id=${current_uboot_id} already present in uboot"
fi

#kernel command line on fccp is built by setenv commands in nandargs var.
#if systemd.machine_id not in nandargs append it
nandargs=$(fw_printenv -n nandargs)
if [ "${nandargs#*systemd.machine_id}" == "${nandargs}" ]; then
    echo "systemd.machine_id arg not present in nandargs var in uboot env, appending now"
    fw_setenv nandargs "${nandargs} systemd.machine_id=\${systemd_machine_id}"
else
    echo "systemd.machine_id already present in nandargs var in uboot env"
fi
