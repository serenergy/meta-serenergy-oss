SUMMARY = "Systemd service for repairing all file systems before mount"
LICENSE = "PD"
LIC_FILES_CHKSUM = "file://${WORKDIR}/pd_license;md5=7246f848faa4e9c9fc0ea91122d6e680"

SRC_URI = " \
    file://fsck-repair-writable.service \
    file://pd_license \
"

inherit systemd

SYSTEMD_SERVICE_${PN} = "fsck-repair-writable.service"

FILES_${PN} += " \
    ${systemd_system_unitdir}/fsck-repair-writable.service \
"

COMPATIBLE_MACHINE = "(fccp)|(rpi3-frontend)"

REQUIRED_DISTRO_FEATURES= "systemd"

do_install() {
    install -Dm 0644 ${WORKDIR}/fsck-repair-writable.service ${D}${systemd_system_unitdir}/fsck-repair-writable.service
}
