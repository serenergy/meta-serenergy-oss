DESCRIPTON = "Serenergy Mount Points"
LICENSE = "PD"
LIC_FILES_CHKSUM = "file://${WORKDIR}/pd_license;md5=7246f848faa4e9c9fc0ea91122d6e680"

SRC_URI = " \
    file://pd_license \
"

do_install() {
    install -d ${D}/usr/local # for apps
    #Create mount points for seperate partitions
    install -d ${D}/log       #for logs
    install -d ${D}/settings  #for persistent settings
    install -d ${D}/sdcard    #for persistent longterm storage
}


FILES_${PN} = " \
    /usr/local \
    /settings \
    /log \
    /sdcard \
"
