SUMMARY = "Systemd service for persisting systemd machine id to uboot env"
LICENSE = "PD"
LIC_FILES_CHKSUM = "file://${WORKDIR}/pd_license;md5=7246f848faa4e9c9fc0ea91122d6e680"

SRC_URI = " \
    file://persist-machine-id.sh \
    file://persist-machine-id.service \
    file://pd_license \
"

inherit systemd

SYSTEMD_SERVICE_${PN} = "persist-machine-id.service"

FILES_${PN} += " \
    ${bindir}/persist-machine-id.sh \
    ${systemd_system_unitdir}/persist-machine-id.service \
"

COMPATIBLE_MACHINE = "(fccp)|(rpi3-frontend)"

REQUIRED_DISTRO_FEATURES= "systemd"

do_install() {
    install -Dm 0755 ${WORKDIR}/persist-machine-id.sh ${D}${bindir}/persist-machine-id.sh
    install -Dm 0644 ${WORKDIR}/persist-machine-id.service ${D}${systemd_system_unitdir}/persist-machine-id.service
}
