SUMMARY = "Systemd service for mounting appfs"
LICENSE = "PD"
LIC_FILES_CHKSUM = "file://${WORKDIR}/pd_license;md5=7246f848faa4e9c9fc0ea91122d6e680"

SRC_URI = " \
    file://mount-appfs.sh \
    file://mount-appfs.service \
    file://pd_license \
"

inherit systemd

SYSTEMD_SERVICE_${PN} = "mount-appfs.service"

FILES_${PN} += " \
    ${bindir}/mount-appfs.sh \
    ${systemd_system_unitdir}/mount-appfs.service \
"

REQUIRED_DISTRO_FEATURES= "systemd"

do_install() {
    install -d ${D}${bindir}
    install -m 0755 ${WORKDIR}/mount-appfs.sh ${D}${bindir}
    install -d ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/mount-appfs.service ${D}${systemd_system_unitdir}
}
