SUMMARY = "Hign level library for implementing snmp++ agents"
HOMEPAGE = "http://www.agentpp.com/"
DEPENDS = "snmp++"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE-2_0.txt;md5=d229da563da18fe5d58cd95a6467d584"

PR = "r1"

SRC_URI = "http://www.agentpp.com/download/${PN}-${PV}.tar.gz"
SRC_URI[md5sum] = "2f87b05bc2442f84fc0d3de07f0e93ef"
SRC_URI[sha256sum] = "ffbd06e6582978b6ea7344ae0ccfe58e634b84d31496bb6eb77f0a9e8f1e1df1"

do_compile_append() {
    for i in $(find ${B} -name "*.pc") ; do
        sed -i -e s:${STAGING_DIR_TARGET}::g \
               -e s:/${TARGET_SYS}::g \
                  $i
    done
}

inherit autotools pkgconfig
