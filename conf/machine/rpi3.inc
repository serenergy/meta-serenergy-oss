#@TYPE: Machine
#@NAME: Serenergy RPI3 CM
#@SOC: BCM2837
#@DESCRIPTION: Machine configuration for Serenergy RPI3 CM POC
#@MAINTAINER: 

require conf/machine/include/arm/arch-armv8a.inc

UBOOT_MACHINE = "rpi_3_ser_defconfig"
UBOOT_BINARY = "u-boot.bin"
UBOOT_ENV_DEVICE = "/boot/uboot.env"
UBOOT_ENV_SIZE = "0x4000"

KERNEL_DEFCONFIG = "rpi3_ser_defconfig"

IMAGE_FSTYPES = "squashfs tar.bz2 cpio.xz wic.gz wic.bmap"

SERIAL_CONSOLE = "115200 ttyAMA0"

# Configure installation of bootfiles and u-boot on first partition
IMAGE_BOOT_FILES = "${UBOOT_BINARY} uboot.env boot.scr rpi-bootfiles/* rpi-config/*"
WKS_FILE_DEPENDS = "virtual/bootloader rpi-bootfiles rpi-config"

MACHINE_EXTRA_RDEPENDS = " \
    kernel-image \
    u-boot-script \
    u-boot-fw-utils \
    rng-tools \
    stress \
    dbus \
    rauc \
    mount-appfs \
    dosfstools \
"

do_image_wic[depends] += " \
    rpi-bootfiles:do_deploy \
    rpi-config:do_deploy \
    u-boot-ser:do_deploy \
"
